# README #

A tool to facilitate Content Engineer GIS tasks.

### How do I get set up? ###

* Requires Python 3.5.
* Clone repository and run 'pip3 install -r requirements.txt'.
* In Windows, run the build.bat file. Pyinstaller must be present.

### Who do I talk to? ###

* Robert Ross Wardrup (minorsecond@gmail.com)